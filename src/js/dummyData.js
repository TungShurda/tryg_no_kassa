var dummyData = {
    "priceData":{
        "orderid":"4b76783e-7dac-4fd2-a413-68250b6a5520",
        "price":{
            "small":{
                "fixed":337.52,
                "leasing":0.00,
                "loan":0.00,
                "tax":0,
                "usage":[
                {
                    "code":"06",
                    "price":304.62,
                    "start":0,
                    "end":6000
                },
                {
                    "code":"08",
                    "price":336.05,
                    "start":6001,
                    "end":8000
                },
                {
                    "code":"10",
                    "price":367.49,
                    "start":8001,
                    "end":10000
                },
                {
                    "code":"12",
                    "price":398.92,
                    "start":10001,
                    "end":12000
                },
                {
                    "code":"14",
                    "price":430.36,
                    "start":12001,
                    "end":14000
                },
                {
                    "code":"16",
                    "price":461.79,
                    "start":14001,
                    "end":16000
                },
                {
                    "code":"18",
                    "price":493.23,
                    "start":16001,
                    "end":18000
                },
                {
                    "code":"20",
                    "price":524.66,
                    "start":18001,
                    "end":20000
                },
                {
                    "code":"25",
                    "price":603.25,
                    "start":20001,
                    "end":25000
                },
                {
                    "code":"30",
                    "price":681.84,
                    "start":25001,
                    "end":30000
                },
                {
                    "code":"40",
                    "price":839.01,
                    "start":30001,
                    "end":40000
                }
                ]
            },
            "medium":{
                "fixed":777.76,
                "leasing":0.00,
                "loan":0.00,
                "tax":0,
                "usage":[
                {
                    "code":"06",
                    "price":1257.19,
                    "start":0,
                    "end":6000
                },
                {
                    "code":"08",
                    "price":1386.93,
                    "start":6001,
                    "end":8000
                },
                {
                    "code":"10",
                    "price":1516.66,
                    "start":8001,
                    "end":10000
                },
                {
                    "code":"12",
                    "price":1646.40,
                    "start":10001,
                    "end":12000
                },
                {
                    "code":"14",
                    "price":1776.13,
                    "start":12001,
                    "end":14000
                },
                {
                    "code":"16",
                    "price":1905.86,
                    "start":14001,
                    "end":16000
                },
                {
                    "code":"18",
                    "price":2035.60,
                    "start":16001,
                    "end":18000
                },
                {
                    "code":"20",
                    "price":2165.33,
                    "start":18001,
                    "end":20000
                },
                {
                    "code":"25",
                    "price":2489.67,
                    "start":20001,
                    "end":25000
                },
                {
                    "code":"30",
                    "price":2814.00,
                    "start":25001,
                    "end":30000
                },
                {
                    "code":"40",
                    "price":3462.67,
                    "start":30001,
                    "end":40000
                }
                ]
            },
            "large":{
                "fixed":817.76,
                "leasing":0.00,
                "loan":0.00,
                "tax":0,
                "usage":[
                {
                    "code":"06",
                    "price":1257.19,
                    "start":0,
                    "end":6000
                },
                {
                    "code":"08",
                    "price":1386.93,
                    "start":6001,
                    "end":8000
                },
                {
                    "code":"10",
                    "price":1516.66,
                    "start":8001,
                    "end":10000
                },
                {
                    "code":"12",
                    "price":1646.40,
                    "start":10001,
                    "end":12000
                },
                {
                    "code":"14",
                    "price":1776.13,
                    "start":12001,
                    "end":14000
                },
                {
                    "code":"16",
                    "price":1905.86,
                    "start":14001,
                    "end":16000
                },
                {
                    "code":"18",
                    "price":2035.60,
                    "start":16001,
                    "end":18000
                },
                {
                    "code":"20",
                    "price":2165.33,
                    "start":18001,
                    "end":20000
                },
                {
                    "code":"25",
                    "price":2489.67,
                    "start":20001,
                    "end":25000
                },
                {
                    "code":"30",
                    "price":2814.00,
                    "start":25001,
                    "end":30000
                },
                {
                    "code":"40",
                    "price":3462.67,
                    "start":30001,
                    "end":40000
                }
                ]
            }
        }
    },
    "customer":{
        "firstName":"Joakim",
        "lastName":"Allen",
        "addr":"Hammarvagen 43",
        "areaCode":"177 35",
        "city":"Stockholm",
        "persNr":"199512164555"
    },
    "vehicle":{
        "regNr":"txs473",
        "vehicleModel":"Toyota",
        "vehicleYear":"2007"
    }
};