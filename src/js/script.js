var $ = require("jquery");
var noUiSlider = require('nouislider');
var wNumb = require('wnumb');
var ionRangeSlider = require("ion-rangeslider");
var flatpickr = require("flatpickr");
var dataValues = {}; 
var formPersInfo = {}

var pkgChoice = "";

chooseInsuranceType();

import validation , {init} from './validation'

var URI = "http://127.0.0.1";
//var URI = "https://trygsidekick.com";

function loadinSpinner(elem, status) {
    /*
        loadinSpinner($('.getPrice'), 1);
    */
    if (status == 1) { // Loading
        var loadingBlock = document.createElement("div");
        loadingBlock.className = "tmpLoadingSpinnerFix";

        document.getElementsByClassName("container")[0].appendChild(loadingBlock);
        elem.find('i').css('color', 'white');
        elem.find('span').hide();


        var spinner = document.createElement("div"),
            bounce_1 = document.createElement("div"),
            bounce_2 = document.createElement("div");
        
        spinner.className = "spinner";
        bounce_1.className = "double-bounce1";
        bounce_2.className = "double-bounce2";

        spinner.appendChild(bounce_1);
        spinner.appendChild(bounce_2);

        elem.append(spinner);

        console.log("Spinner appended");
        var lmao = "break point";

    } else { // Finished
        $(".tmpLoadingSpinnerFix").remove();
        elem.find('i').css('color', 'black');
        elem.find('span').show();
        elem.find('.spinner').remove();
    }
    //toggleEvents.toggle();
}

// Set value of checkbox
$('.checkValue').on('change', function(){
    this.value = this.checked ? 1 : 0;
 }).change();

function saveForm(){

    var ssn = $("#fNumber").val();
    var firstName = $("#fNamn").val();
    var lastName = $("#eNamn").val();
    var regNumber = $("#regNr").val();
    var pant = $("#pant").val();
    var leasing = $("#leasing").val(); 
    
    if ($("#pant").is(":checked"))
        pant = true;
    else 
        pant = false;

    if ($("#leasing").is(":checked"))
        leasing = true;
    else 
        leasing = false;

    formPersInfo ["ssn"] = ssn;
    formPersInfo ["firstName"] = firstName;
    formPersInfo ["lastName"] = lastName;
    formPersInfo ["regNumber"] = regNumber;    
    formPersInfo ["pant"] = pant;
    formPersInfo ["leasing"] = leasing;            
    

    $("#fodelsenummer").text( formPersInfo.ssn );
    $("#fornavn").text( formPersInfo.firstName );
    $("#etternavn").text( formPersInfo.lastName );
    $("#carRegNr").text( formPersInfo.regNumber );   
};

var initService = {
    requestObj: {
        "Car": {
            "RegistrationNumber": "",
            "CarModelCode": "",
            "CarModelText": "",
            "CarModelYear": "",
            "VIN": ""
        },
        "Customer": {
            "Ssn": "",
            "FirstName": "",
            "LastName": "",
            "EmailAddress": "",
            "MobilePhone": "",
            "CustomerId": ""
        },
        "OtherOwner": {
            "BirthDate": "",
            "FirstName": "",
            "LastName": ""
        },
        "InsuranceCompany": {
            "InsuranceCompanyCode": "",
            "InsuranceCompanyText": ""
        },
        "ExpectedMileage": "",
        "Leasing": "",
        "Pant": "",
        "Comments": "",
        "Reason": "",
        "RegNoExistingCar": "",
        "Bonus": "",
        "DistanceCode": "",
        "CoverPackage": ""
    },
    fetch: function () {
        const this_ = this;
        loadinSpinner($('.orderIns'), 1);
        $.ajax({
            headers: { 
                'Accept': 'application/json',
                'Content-Type': 'application/json' 
            },
            'type': 'POST',
            'url': URI + "/api/tryggNo/init",
            'data': JSON.stringify(initService.requestObj),
            'dataType': 'json',
            'success': function (resp) {
                if (resp.netUrl)
                    window.location.href = resp.netUrl;
                console.log(resp);
            }
        }).always(function() {
            loadinSpinner($('.orderIns'), 0);
        });;
    },
    init: function () {
        this.requestObj.Car = priceQuoteService.requestObj.Car;

        this.requestObj.Customer.Ssn = formPersInfo.ssn;
        this.requestObj.Customer.FirstName = formPersInfo.firstName;
        this.requestObj.Customer.LastName = formPersInfo.lastName;
        this.requestObj.Customer.EmailAddress = formPersInfo.email;
        this.requestObj.Customer.MobilePhone = formPersInfo.phoneNumber;

        this.requestObj.OtherOwner.BirthDate = formPersInfo.otherSsn;
        this.requestObj.OtherOwner.FirstName = formPersInfo.otherFirstName;
        this.requestObj.OtherOwner.LastName = formPersInfo.otherLastName;
        
        this.requestObj.InsuranceCompany.InsuranceCompanyCode = formPersInfo.bonusTransfer;

        var insText = "";
        for (var x in definitionData.InsuranceCompanies) {
            if (definitionData.InsuranceCompanies[x].insuranceCompanyCode == formPersInfo.bonusTransfer) {
                insText = definitionData.InsuranceCompanies[x].insuranceCompanyText;
            }
        }

        this.requestObj.InsuranceCompany.InsuranceCompanyText = insText;


        this.requestObj.ExpectedMileage = formPersInfo.DistanceCode;//formPersInfo.milesYear;
        this.requestObj.Leasing = formPersInfo.leasing;
        this.requestObj.Pant = formPersInfo.pant;
        this.requestObj.Comments = "kommentar"
        this.requestObj.Reason = "6"//formPersInfo.bonusTransfer;
        this.requestObj.RegNoExistingCar = "ingen"
        this.requestObj.Bonus = formPersInfo.bonus;
        this.requestObj.DistanceCode = formPersInfo.DistanceCode;
        this.requestObj.CoverPackage = formPersInfo.CoverPackage;
        this.requestObj.ActivationDate = formPersInfo.ActivationDate;

        this.fetch();
    }
}

$("#acceptOrder").click(function() {      


    if (init(".step4block1")) {
        var address = $("#adresseEdit").val();
        var zipcode = $("#postnummerEdit").val();
        var carBrand = $("#carBrand").val();
        var yearModel = $("#carYear").val();
        var phoneNumber = $("#phoneNumber").val();
        var email = $("#eMail").val();
        var bonus = $("#bonusDropDown").val();
        var accessReason = $("#tilgangsorsak").val();   
        var bonusTransfer = $("#overforesFra").val();    
        var leasingCompany = $("#leasingbolag").val();
        var otherFirstName = $("#annanEierFname").val();
        var otherLastName = $("#annanEierLname").val();
        var otherSsn = $("#annanEierPn").val();  
        var ActivationDate = $("#startDate").val();  
        
        var addrObj = priceData.resp.getPriceQuoteRespItem.body.getPriceQuoteResponse.address;
        var carObj = priceData.resp.getPriceQuoteRespItem.body.getPriceQuoteResponse.car;
        
        formPersInfo ["address"] = address || addrObj.streetAddress;
        formPersInfo ["zipcode"] = zipcode || addrObj.postalCode;
        //formPersInfo ["carBrand"] = carBrand;
        //formPersInfo ["yearModel"] = yearModel;
        //formPersInfo ["milesYear"] = milesYear;
        formPersInfo ["phoneNumber"] = phoneNumber;
        formPersInfo ["email"] = email;
        formPersInfo ["bonus"] = bonus;
        formPersInfo ["accessReason"] = accessReason;
        formPersInfo ["bonusTransfer"] = bonusTransfer;    
        formPersInfo ["leasingCompany"] = leasingCompany;   
        formPersInfo ["otherFirstName"] = otherFirstName;
        formPersInfo ["otherLastName"] = otherLastName;   
        formPersInfo ["otherSsn"] = otherSsn;       
           
        formPersInfo ["ActivationDate"] = ActivationDate; 
              
        
        console.log(formPersInfo);
        initService.init(); 
    } else {
        console.log("yo")
    }
    
    
});




var priceQuoteService = {
    requestObj: {
        "Car": {
            "RegistrationNumber": "",
            "CarModelCode": "",
            "CarModelText": "",
            "CarModelYear": "",
            "VIN": ""
        },
        "Customer": {
            "Ssn": "",
            "FirstName": "",
            "LastName": "",
            "EmailAddress": "",
            "MobilePhone": "",
            "CustomerId": ""
        }
    },
    init: function () {
        this.requestObj.Car.RegistrationNumber = formPersInfo.regNumber;
        this.requestObj.Customer.Ssn = formPersInfo.ssn;
        this.requestObj.Customer.FirstName = formPersInfo.firstName;
        this.requestObj.Customer.LastName = formPersInfo.lastName;

    }
}

$("#step1Button").click(function() {        
    if (init(".step1")) { //validationSsn()){
        saveForm();
        priceQuoteService.init();
        console.log(priceQuoteService.requestObj)
        priceData.init();
    }
});


var sliders = {
	elems: {
		gradeSlider: null,
		gradeValue: null
	},
	objects: {
		gradeSlider: null
    },
    currentVal: null,
    currentCode: null,
	bindSliderToInput: function () {
		var this_ = this;
		this.elems.gradeSlider[0].noUiSlider.on('update', function() {
			this_.elems.gradeValue.text( this_.elems.gradeSlider[0].noUiSlider.get() );
			calculate.calcGrade(this_.elems.gradeSlider[0].noUiSlider.get());
		});

	},
	init: function (priceData) {

        const this_ = this;

        $("#example_id").ionRangeSlider({
            grid: true,
            values: priceData.map((price, i, prices) => {
                if(price.end == 6000)
                    return 0
                return price.end
            }),
            from: 2,
            onChange: function(data) {
                this_.currentCode = priceData.filter((price, i, prices) => {
                    return price.end == data.from_value
                });
                this_.currentVal = data.from_value;
                var code = this_.currentCode[0].code; 
                setPrices(code);
                setDataStep3();
                formPersInfo.DistanceCode = code;
            },
            onStart: function(data) {
                this_.currentCode = 10;
                this_.currentVal = 10000;
                setPrices(this_.currentCode);
                setDataStep3();
                formPersInfo.DistanceCode = this_.currentCode;
            }
        });
        

	}
};

var priceData =  {
    resp: null,
    prices: null,
    init: function (persNr, regNr) {
        const this_ = this;

        if (this_.resp != null && this_.resp.getPriceQuoteRespItem.body.getPriceQuoteResponse.carSuggestions.car.length > 0) {
            var carCode = $("#bilmodell").val();
            var cars = this_.resp.getPriceQuoteRespItem.body.getPriceQuoteResponse.carSuggestions.car;
            for (var i in cars) {
                if (carCode == cars[i].carModelCode) {
                    priceQuoteService.requestObj.Car.CarModelCode = cars[i].carModelCode;
                    priceQuoteService.requestObj.Car.CarModelText = cars[i].carModelText;
                    priceQuoteService.requestObj.Car.CarModelYear = cars[i].carModelYear;
                    priceQuoteService.requestObj.Car.RegistrationNumber = formPersInfo.regNumber;
                }        
            }

            //priceQuoteService.requestObj.Car.CarModelCode
        }

        loadinSpinner($('.getPrice'), 1);
        $.ajax({
            headers: { 
                'Accept': 'application/json',
                'Content-Type': 'application/json' 
            },
            'type': 'POST',
            'url': URI + "/api/tryggNo/priceQuote",
            'data': JSON.stringify(priceQuoteService.requestObj),
            'dataType': 'json',
            'success': function (resp) {
                
                this_.resp = resp;

                if (resp.spTrygSideKickPrewebRespItem != null) {
                    this_.prices = JSON.parse(resp.spTrygSideKickPrewebRespItem.price);
                    $(".step2").css("display", "block");
                    $(".step3").css("display", "block");    
                
                    $('html, body').animate({
                        scrollTop: $(".step2").offset().top
                    }, 800); 
                    
    
                    console.log(this_.prices)
                    setPrices();
    
                    setDataStep3();
                    sliders.init(this_.prices.small.usage);
                } else {
                    $(".bilmodell").show();
                    var cars = resp.getPriceQuoteRespItem.body.getPriceQuoteResponse.carSuggestions.car;
                    for (var i in cars) {
                        $("#bilmodell").append(new Option(cars[i].carModelText, cars[i].carModelCode, true, true));           
                    }
                }

                
            }
        }).always(function() {
            loadinSpinner($('.getPrice'), 0);
        });
    }
}

var definitionData =  {
    resp: null,
    prices: null,
    carTypes: null,
    InsuranceCompanies: null,
    init: function () {
        const this_ = this;

        //loadinSpinner($('.getPrice'), 1);
        $.ajax({
            headers: { 
                'Accept': 'application/json',
                'Content-Type': 'application/json' 
            },
            'type': 'GET',
            'url': URI + "/api/tryggNo/definitionData",
            'dataType': 'json',
            'success': function (resp) {
                
                this_.resp = resp;
                this_.carTypes = resp.getDefinitionData.body.getDefinitionDataResponse.carTypes.carType;
                this_.InsuranceCompanies = resp.getDefinitionData.body.getDefinitionDataResponse.insuranceCompanies.insuranceCompany;
                
                //addDropDownValues(carType, "#bilmodell");

                var carTypeDropDownList = [];
                for (var x in this_.carTypes) {
                    carTypeDropDownList.push({
                        name: this_.carTypes[x].carText,
                        id: this_.carTypes[x].carCode
                    });
                }

                var InsuranceDropDownList = [];
                for (var x in this_.InsuranceCompanies) {
                    InsuranceDropDownList.push({
                        name: this_.InsuranceCompanies[x].insuranceCompanyText,
                        id: this_.InsuranceCompanies[x].insuranceCompanyCode
                    });
                }

                addDropDownValues(InsuranceDropDownList, "#overforesFra");
                addDropDownValues(carTypeDropDownList, "#bilmodell");

                /* function addSellskap() {
                    var sellskap = ["ACE European Group Limited", "Agria Dyreforsikring", "AIOI Insuranse Co.of Europe", "Atlantica Båtforsikring", "Besure Forsikring Norge As", "Bilsport Og Mc Spesialfors", "BS Forsikring", "Byggmesterforsikring As", "Chartis", "Codan Forsikring ", "DNB Livforsikring ASA", "DNB Skadeforsikring AS", "Eika Forsikring AS", "Enter Forsikring ", "ETU Forsikring As", "Euro Insurances", "Europeiske Reiseforsikring", "Frende Skadeforsikring AS", "Frisk Forsikring", "Gjensidige Forsikring", "Gouda Reiseforsikring", "If Skadeforsikring", "Inter Hannover / W.L.I.ASA", "Jernbanenepers. Fors. Gjensidige", "KLP Skadeforsikring AS", "KNIF", "Landbruksforsikring As", "Liveo Forsikring", "Lyberg Og Partnere As", "Moderna Forsikringer Sak Ab", "Møretrygd", "Naf Forsikring As", "NEMI Forsikring ASA", "Netviq", "Nordenfjeldske Forsikring", "Nordeuropa Forsikring As", "Nordic Insurance", "Norsk Caravan Club", "Norske Sjø AS", "Norwegian Broker As", "Protector Forsikring ASA", "Quality Maritime Ins. As", "Saga Forsikring As", "Scandinavian Insuranse Group", "Smb Forsikring As", "Sparebank1 Skadeforsikring AS", "Storebrand Skadeforsikring AS", "Tennant Forsikring NUF", "Troll Forsikring", "Tryg Forsikring", "Trygg Hansa", "Unison Forsikring Asa", "Vardia Forsikring As", "Vertikal", "Virke Forsikring As", "Volvia Forsikring", "Watercircles Norge", "White Label Insurance ASA", "Winterbergh Forsikring"];
                
                    for (var i in sellskap) {
                        $("#overforesFra").append(new Option(sellskap[i], sellskap[i].replace(" ", "") , true, true));           
                    }
                
                    $("#overforesFra").prop("selectedIndex", 0);  
                } */

                
                
                // definitionData.init();
            }
        }).always(function() {
            //loadinSpinner($('.getPrice'), 0);
        });
    }
}
definitionData.init();
function setPrices (code) {

    if (!code)
        code = "06";

    var smallPkg = priceData.prices.small.usage.filter((usage, i, usages) => {
        return usage.code == code
    });

    var mediumPkg = priceData.prices.medium.usage.filter((usage, i, usages) => {
        return usage.code == code
    });

    var largePkg = priceData.prices.large.usage.filter((usage, i, usages) => {
        return usage.code == code
    });

    console.log("small", smallPkg);
    console.log("medium", mediumPkg);
    console.log("large", largePkg);

    dataValues.set1ValTraficInsurance = smallPkg[0].price;
    dataValues.set2ValTraficInsurance = mediumPkg[0].price;
    dataValues.set3ValTraficInsurance = largePkg[0].price;

    dataValues.set1ValInsurance = priceData.prices.small.fixed;
    dataValues.set1Total = dataValues.set1ValInsurance + dataValues.set1ValTraficInsurance;
    dataValues.set1Total = Math.ceil( dataValues.set1Total );

    //set2
    dataValues.set2ValInsurance = priceData.prices.medium.fixed;
    dataValues.set2Total = dataValues.set2ValInsurance + dataValues.set2ValTraficInsurance;
    dataValues.set2Total = Math.ceil( dataValues.set2Total );

    //set3
    dataValues.set3ValInsurance = priceData.prices.large.fixed;
    dataValues.set3Total = dataValues.set3ValInsurance + dataValues.set3ValTraficInsurance;
    dataValues.set3Total = Math.ceil( dataValues.set3Total );
}

// function validationStep1 (){
//     if (validationName()){
//         validationSsn();
//     }
//     else{
//         console.log("error");
//     }
// }

//Check validation SSN
function validationSsn() {

    var validationStatus = false;
    
    new RegExp("[^0-9]{11}");   
    var regex = new RegExp("/^([0-9]{11}|[0-9]{6}\-[0-9]{5})$/");

    var regex_ssn_code = new RegExp("^([0-9]{11}|[0-9]{6}\-[0-9]{5})$");
    var ssn_val = document.getElementById("fNumber").value;

    if (regex_ssn_code.test(ssn_val)) {
        validationStatus = true;
        
    } else {
        wrongPnPop();
    }  
    return validationStatus;
}

//Check validation Email
function validationEmail() {
    
    var regex_email_code = new RegExp("/^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,6}$/");
    var email_val = document.getElementById("eMail").value;

    if (regex_email_code.test(email_val)) {
        console.log("GG epost");
        
    } else {
        wrongEmailPop();
    }  
}

//Check validation Name
function validationName() {

    var firstName=$.trim($("#fNamn").val());
    var lastName=$.trim($("#eNamn").val());
     
    if(firstName.length>0 && lastName.length>0){
    }
    else{
        wrongNamePop();     
    }
}

// $(function activateInputCss () {   
//     if($('#fNumber').val()){
//         console.log(1);
//     }
// });
// $(function() {
//     $('#fNumber').bind('change', function (e) { 
//         $("#fNumber").css(".bar:before .bar:after");
//     }).trigger('change');
// });
// $('#fNumber').on('input',function(e){
// });


//Set price insurance choice
function setDataStep3() {

    //set1
    $("#kaskoExtraYourPrice").text( dataValues.set3ValInsurance );
    $("#kaskoExtraStaten").text( dataValues.set3ValTraficInsurance );
    $("#kaskoExtraTotal").text( dataValues.set3Total );

    //set2 
    $("#kaskoYourPrice").text( dataValues.set2ValInsurance );
    $("#kaskoStaten").text( dataValues.set2ValTraficInsurance );
    $("#kaskoTotal").text( dataValues.set2Total);

    //set3
    $("#kaskoDelYourPrice").text( dataValues.set1ValInsurance );
    $("#kaskoDelStaten").text( dataValues.set1ValTraficInsurance );
    $("#kaskoDelTotal").text( dataValues.set1Total );


    var addrObj = priceData.resp.getPriceQuoteRespItem.body.getPriceQuoteResponse.address;
    var carObj = priceData.resp.getPriceQuoteRespItem.body.getPriceQuoteResponse.car;
    // Other
    $("#adresse").text( addrObj.streetAddress );
    $("#postnummer").text( addrObj.postalCode + " " + addrObj.city );

    formPersInfo ["address"] = addrObj.streetAddress;
    formPersInfo ["zipcode"] = addrObj.postalCode;

    $("#carBrand").text( carObj.carModelText );
    $("#carYear").text( carObj.carModelYear );

    formPersInfo ["yearModel"] = carObj.carModelYear;
    formPersInfo ["carBrand"] = carObj.carModelText;

    $("#driveRange").text( sliders.currentVal + " km pr år" );

    formPersInfo ["milesYear"] = sliders.currentVal;

};
    
//Choose insurance
function chooseInsuranceType() {

    $("#insuranceType1").click(function() { 
        $(".step4").css("display", "block");
        $(".step4block1").css("display", "block");
        $(".step4block2").css("display", "block");
        $(".order").css("display", "block");        
        $(".step1").css("display", "none");
        $(".step2").css("display", "none");
        $(".step3").css("display", "none");    

        $("#choosenYourPrice").text( dataValues.set1ValInsurance );
        $("#choosenStatenPrice").text( dataValues.set1ValTraficInsurance );
        $("#choosenTotal").text( dataValues.set1Total );
        $("#choosenKaskoh1").text("Kasko Ekstra"); 
        $('.choosenKasko').css('background-color','#ce0303');  

        $('html, body').animate({
        	scrollTop: $(".container").offset().top
    	}, 200); 
        formPersInfo.CoverPackage = "small";
        saveForm();             
    });
    
    $("#insuranceType2").click(function() {
        $(".step4").css("display", "block");
        $(".step4block1").css("display", "block");
        $(".step4block2").css("display", "block");
        $(".order").css("display", "block");                
        $(".step1").css("display", "none");
        $(".step2").css("display", "none");
        $(".step3").css("display", "none"); 
        
        $("#choosenYourPrice").text(dataValues.set2ValInsurance);
        $("#choosenStatenPrice").text(dataValues.set2ValTraficInsurance);
        $("#choosenTotal").text(dataValues.set2Total);
        $("#choosenKaskoh1").text("Kasko");    
        $('.choosenKasko').css('background-color','#5a5a5a'); 

        $('html, body').animate({
        	scrollTop: $(".container").offset().top
    	}, 200); 
        formPersInfo.CoverPackage = "medium";
        saveForm();           
    });

    $("#insuranceType3").click(function() {
        $(".step4").css("display", "block");
        $(".step4block1").css("display", "block");
        $(".step4block2").css("display", "block");
        $(".order").css("display", "block");                
        $(".step1").css("display", "none");
        $(".step2").css("display", "none");
        $(".step3").css("display", "none");   
        
        $("#choosenYourPrice").text(dataValues.set3ValInsurance);
        $("#choosenStatenPrice").text(dataValues.set3ValTraficInsurance);
        $("#choosenTotal").text(dataValues.set3Total);
        $("#choosenKaskoh1").text("Delkasko");  
        $('.choosenKasko').css('background-color','#999999'); 

        $('html, body').animate({
        	scrollTop: $(".container").offset().top
    	}, 200); 
        formPersInfo.CoverPackage = "large";
        saveForm();                     
    });
}


// Choose "Pant" or "Leasing" or none

$(function checkboxChange () {

	$('input[name="pantOrLeasing"]').on('change', function() {
	   $('input[name="pantOrLeasing"]').not(this).prop('checked', false);
	});

});


// Popup ? on "Beregn uten regnr"

$(function carModelPop () {

	$('.carBrandOrRegnrQ').click(function() {
		$('#carBrandOrRegnrPop').show();
	});

	$('#carBrandOrRegnrPop').find(".close").click(function() {
		$('#carBrandOrRegnrPop').hide();
	});

	$('#carBrandOrRegnrPop').find(".ok").click(function() {
		$('#carBrandOrRegnrPop').hide();
	});

});


// // Popup "Wrong Personalnumber Pop"
function wrongPnPop () {
    $('#wrongPersonalnumberPop').show();

    $('#wrongPersonalnumberPop').find(".close").click(function() {
		$('#wrongPersonalnumberPop').hide();
	});

	$('#wrongPersonalnumberPop').find(".ok").click(function() {
		$('#wrongPersonalnumberPop').hide();
	});
};

// // Popup "Wrong Email Pop"
function wrongEmailPop () {
    $('#wrongEmailPop').show();

    $('#wrongEmailPop').find(".close").click(function() {
		$('#wrongEmailPop').hide();
	});

	$('#wrongEmailPop').find(".ok").click(function() {
		$('#wrongEmailPop').hide();
	});
};

// // Popup "Wrong Name Pop"
function wrongNamePop () {
    $('#wrongNamePop').show();

    $('#wrongNamePop').find(".close").click(function() {
		$('#wrongNamePop').hide();
	});

	$('#wrongNamePop').find(".ok").click(function() {
		$('#wrongNamePop').hide();
	});
};

// // Popup ? on "Other Address"
$(function adressPop () {

	$('.adressQ').click(function() {
		$('#adressPop').show();
	});

 	$('#adressPop').find(".close").click(function() {
 		$('#adressPop').hide();
 	});

 	$('#adressPop').find(".ok").click(function() {
 		$('#adressPop').hide();
 	});

});


// Popup ? on "Bonus"
$(function bonusPop () {

	$('.bonusQ').click(function() {
		$('#bonusPop').show();
	});

	$('#bonusPop').find(".close").click(function() {
		$('#bonusPop').hide();
 	});

 	$('#bonusPop').find(".ok").click(function() {
		$('#bonusPop').hide();
	});

});


// Popup ? on "Other owner"

$(function ownerPop () {

	$('.annanEierQ').click(function() {
		$('#annanEierPop').show();
	});

	$('#annanEierPop').find(".close").click(function() {
		$('#annanEierPop').hide();
	});

	$('#annanEierPop').find(".ok").click(function() {
		$('#annanEierPop').hide();
	});

});


// Popup ? on "Confirm order"

$(function confirmPop () {

	$('.bekreftBestillningQ').click(function() {
		$('#bekreftBestillningPop').show();
	});

	$('#bekreftBestillningPop').find(".close").click(function() {
		$('#bekreftBestillningPop').hide();
	});

	$('#bekreftBestillningPop').find(".ok").click(function() {
		$('#bekreftBestillningPop').hide();
	});

});


// Show inputs for other owner if checkbox checked

$(function showOtherOwner () {

	$(".omAnnanEier").hide();
	
	$(".otherEier").click(function() {
    	if($(this).is(":checked")) {
        	$(".omAnnanEier").show();
    	} else {
        	$(".omAnnanEier").hide();
    	}
	});

});


// Go back

$(function goBack () {

	$('.backStep').click(function() {
		$('.step4').hide();
		$('.step4block1').hide();
		$('.step4block2').hide();
		$('.order').hide();
		$('.step1').show();
		$('omAnnanEier').hide();
	});

});



// Edit address

$(function editAddress () {

	$('.editAddress').click(function() {
		$('.adressEdit').show();
		$('.adressNoEdit').hide();

		$('.editAddress').hide();
		$('.saveEditAddress').show();

    });

});

$(function saveAddress () {

	$('.saveEditAddress').click(function() {
		$('.adressNoEdit').show();
		$('.adressEdit').hide();

		$('.editAddress').show();
		$('.saveEditAddress').hide();

    });

});




$(document).ready( function() {
      $('#tilgangsorsak').bind('change', function (e) { 
        if( $('#tilgangsorsak').val() == 'firstCar') {
        	$('.annetSelskap').hide();
        	$('.overforesFra').hide();
        }
        else if( $('#tilgangsorsak').val() == 'annetSelskap') {
        	$('.annetSelskap').show();
        	$('.overforesFra').show();
        }
        else if( $('#tilgangsorsak').val() == 'newCar') {
        	$('.annetSelskap').hide();
        	$('.overforesFra').show();
        }
        else if( $('#tilgangsorsak').val() == 'bilPark') {
        	$('.annetSelskap').hide();
        	$('.overforesFra').show();
        }       
      }).trigger('change');

      //addSellskap()
      //sliders.init();
      flatpickr("#startDate", {});
      currentDate();
    });



$("#saveEditAddress").click(function() {
    $("#adresse").text($("#adresseEdit").val());
    $("#postnummer").text($("#postnummerEdit").val());
});
// Show inputs for other owner if checkbox checked

$(function beregnUtanRegNo () {

	$(".bilmodell").hide();
	
	$(".noRegNr").click(function() {
    	if($(this).is(":checked")) {
        	$(".bilmodell").show();
        	$(".regno").hide();
        	$(".velgBtn").hide();
    	} else {
        	$(".bilmodell").hide();
        	$(".regno").show();
        	$(".velgBtn").show();
    	}
	});

});


function addSellskap() {
	var sellskap = ["ACE European Group Limited", "Agria Dyreforsikring", "AIOI Insuranse Co.of Europe", "Atlantica Båtforsikring", "Besure Forsikring Norge As", "Bilsport Og Mc Spesialfors", "BS Forsikring", "Byggmesterforsikring As", "Chartis", "Codan Forsikring ", "DNB Livforsikring ASA", "DNB Skadeforsikring AS", "Eika Forsikring AS", "Enter Forsikring ", "ETU Forsikring As", "Euro Insurances", "Europeiske Reiseforsikring", "Frende Skadeforsikring AS", "Frisk Forsikring", "Gjensidige Forsikring", "Gouda Reiseforsikring", "If Skadeforsikring", "Inter Hannover / W.L.I.ASA", "Jernbanenepers. Fors. Gjensidige", "KLP Skadeforsikring AS", "KNIF", "Landbruksforsikring As", "Liveo Forsikring", "Lyberg Og Partnere As", "Moderna Forsikringer Sak Ab", "Møretrygd", "Naf Forsikring As", "NEMI Forsikring ASA", "Netviq", "Nordenfjeldske Forsikring", "Nordeuropa Forsikring As", "Nordic Insurance", "Norsk Caravan Club", "Norske Sjø AS", "Norwegian Broker As", "Protector Forsikring ASA", "Quality Maritime Ins. As", "Saga Forsikring As", "Scandinavian Insuranse Group", "Smb Forsikring As", "Sparebank1 Skadeforsikring AS", "Storebrand Skadeforsikring AS", "Tennant Forsikring NUF", "Troll Forsikring", "Tryg Forsikring", "Trygg Hansa", "Unison Forsikring Asa", "Vardia Forsikring As", "Vertikal", "Virke Forsikring As", "Volvia Forsikring", "Watercircles Norge", "White Label Insurance ASA", "Winterbergh Forsikring"];

	for (var i in sellskap) {
        $("#overforesFra").append(new Option(sellskap[i], sellskap[i].replace(" ", "") , true, true));           
    }

    $("#overforesFra").prop("selectedIndex", 0);  
}


function addDropDownValues(list, selectElem) {
	for (var i in list) {
        var item = list[i];
        $(selectElem).append(new Option(list[i].name, list[i].id , true, true));           
    }

    $(selectElem).prop("selectedIndex", 0);  
}


//Set value when "Min første bil" is selected  
$(function() {
    $('#tilgangsorsak').bind('change', function (e) { 
      if( $('#tilgangsorsak').val() == 'firstCar') {
        $("#bonusDropDown").prop("selectedIndex", 1);   
      }      
    })
});

function currentDate() {
    var currDate = new Date(),
        year = currDate.getFullYear(),
        month = currDate.getMonth()+1,
        day =  currDate.getDate();

    if (day < 10)
        day = "0" + day;
    
    if (month < 10)
        month = "0" + month;

    var formatedDate = year + "-" + month + "-" + day;
    if (!$('#startDate').val()) {
        $('#startDate').val(formatedDate);
    }
}