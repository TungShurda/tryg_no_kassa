var $ = require("jquery");

var globalData = {};

var data = {
    'activationDate': '',
    'email': '',
    'number': '',
    'persNr': '',
    'regNr': '',
    'verifyEmail': ''
}

const fieldValidators = {
    'form-require': {
        'empty': {
            'validator': function () {
                var nullValues = ["", 0, null];
                for (var x in nullValues)
                    if(this.val() == nullValues[x])
                        return false;
                return true;
            },
            'msg': 'Detta fält får ej vara tomt'
        }
    },
    'form-persNr': {
        'format': {
            'validator': function () {
                var match = false,
                    regex = [
                    /([0-9]{11})\d/g, // 1995 without dash
                    /([0-9]{7})\d-([0-9]{3})\d/g, // 1995 with dash
                    /([0-9]{5})\d-([0-9]{3})\d/g, // 95 with dash
                    /([0-9]{9})\d/g // 95 without dash
                ];
                for (var x in regex)
                    if ( regex[x].test(this.val()) )
                        return true;
                return false;
            },
            'msg': 'Personnumret stämmer ej, fel format! (T.ex. 19550505-XXXX)'
        }
    },
    'form-ssn': {
        'format': {
            'validator': function () {
                var match = false,
                    regex = [
                    /^([0-9]{11})/g, // without dash
                    /^([0-9]{6})-([0-9]{5})/g, // with dash
                ];
                for (var x in regex)
                    if ( regex[x].test(this.val()) )
                        return true;
                return false;
            },
            'msg': 'Fødselsnummer stämmer ej, fel format! (T.ex. 19550505-XXXX)'
        }
        /* 'length': {
            'validator': function () {
                return this.val().length == 11 || this.val().length == 12;
            },
            'msg': 'Fødselsnummer stämmer ej, fel format! (T.ex. 19550505-XXXX)'
        } */
    },
    'form-regNr': {
        'stringMinMaxLength': {
            'validator': function () {
                var val = this.val().replace(" ", "");
                return ( (val.length > 2 && val.length < 7) );
            },
            'msg': 'Reg nr måste vara mellan 2 och 7 siffror'
        },
        'format': {
            'validator': function () {
                return !/[^A-Za-z0-9\s]/.test(this.val());
            },
            'msg': 'Reg nr får endast innehålla siffror och bokstäver'
        }
    },
    'form-email': {
        'regex': {
            'validator': function () {
                return emailRegex().test(this.val());
            },
            'msg': 'Email stämmer ej'
        }
    },
    'form-phone': {
        'number': {
            'validator': function () {
                return (!isNaN(this.val()));
            },
            'msg': 'Detta fält får endast vara siffror'
        },
        'format': {
            'validator': function () {
                return /^\+?[1-9]\d{1,14}$/.test(this.val());
            },
            'msg': 'Fel format på nummret'
        }
    },
    'form-countryCode': {
        'code': {
            'validator': function () {
                for (var x in a_.phone) {
                    var val = this.val().split("+")[1] || this.val();
                    if (a_.phone[x] == val) {
                        return true;
                    }
                }
                return false;
            },
            'msg': 'Måste vara ett Landsnummer'
        }
    },
    'form-date': {
        'valid': {
            'validator': function () {
                return ( new Date( this.val()) !== "Invalid Date" );
            },
            'msg': 'Felaktigt datum'
        },
        'activation': {
            'validator': function () {
                return ( new Date(this.val()).setHours(0,0,0,0) >= new Date().setHours(0,0,0,0) );
            },
            'msg': 'Försäkringen kan börja tidigast från och med idag.'
        },
        'activation1': {
            'validator': function () {

                /* if ($("#tilgangsorsak").val() == "annetSelskap") {
                    var nullValues = ["", 0, null];
                    for (var x in nullValues)
                        if($("#overforesFra").val() == nullValues[x])
                            return false;
                    return true;
                } */

                if ($("#tilgangsorsak").val() == "annetSelskap") {
                    var futureDate = new Date();
                    return ( new Date(this.val()).setHours(0,0,0,0) >= futureDate.setDate(futureDate.getDate() + 30) );
                }
                return true;
            },
            'msg': 'Försäkringen kan börja tidigast från och med om 30 dagar.'
        }
    }
}

function onPageValidation (section) {
    var _this = this;

    //Clear
    //$('span.error').empty();
    //$('.field').removeClass('error');
    data = {};
    var formValid = true;
    
    
    $(section).find(".field").each(function () {

        var $field = $(this),
            $inputFields = $(this).find('input, select'),
            errors = [],
            errMsg = "";

        $inputFields.each(function () {
            var $input = $(this);

            for (var x in fieldValidators)
                if ($input.hasClass(x))
                    for (var y in fieldValidators[x]) {
                        var valid = fieldValidators[x][y].validator.call($input);
                        if (!valid)
                            errors.push( fieldValidators[x][y].msg );
                    }

            if (errors.length) {
                formValid = false;
                for (var x in errors)
                    errMsg += errors[x] + "<br>";
                $field.find('.error').html( errMsg );
                $field.addClass('error');
                $input.change(function(){
                    $field.removeClass("error");
                    $field.find('.error').text("")
                    $(this).unbind();
                });
            } else {

                var name = $input.attr('name'),
                    val = $input.val();
                
                if (name == "persNr")
                    data[name] = val.replace("-", "");

                else if (name == "regNr")
                    data[name] = val.replace(" ", "");

                else if (name == "countryCode")
                    data['number'] = val;

                else if (name == "number")
                    data[name] += val;
                else
                    data[name] = val;
                

                //if ( globalData.data.collected.form.hasOwnProperty(name) )
                //    globalData.data.collected.form[name] = data[name];

            }

        });
    });

    console.log(formValid)

    return formValid;
}

function init (section) {
    if (onPageValidation(section)) {
        return true;
    } else {
        return false;
    }
}

function emailRegex(){
    return /^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
}

export {init}