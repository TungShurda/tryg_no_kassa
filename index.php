<html lang="no">
<head>
    <link rel="stylesheet" href="dist/style/main.css">
	<link rel="stylesheet" href="dist/style/flatpickr.min.css">
	
	<!--[if IE 9]>
	<link rel="stylesheet" href="dist/style/ie.css">
	<![endif]-->

    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.2.0/css/ion.rangeSlider.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ion-rangeslider/2.2.0/css/ion.rangeSlider.skinFlat.css">

	<script
  src="https://code.jquery.com/jquery-3.2.1.min.js"
  integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
  crossorigin="anonymous"></script>
</head>
<body>

	<div class="container">

        <!-- Felmeddelande: Fel personnummer -->
        <div class="popup" id="wrongPersonalnumberPop">
            <div class="close">x</div>

            <h3>Du har ikke skrevet fødselsnummeret rett</h3>
            
            <div class="description">
                <p>Fødselsnummeret ditt består av 11 siffer (ddmmååxxxxx). Det er helt trygt å legge inn denne informasjonen.</p>
            </div>
            <a href="#" class="ok">Ok</a>
		</div>
		
		<!-- Felmeddelande: Fel Email -->
		<div class="popup" id="wrongEmailPop">
            <div class="close">x</div>

            <h3>Du har ikke skrevet email-adresse rett</h3>
            
            <a href="#" class="ok">Ok</a>
		</div>
		
		<!-- Felmeddelande: Fel Email -->
		<div class="popup" id="wrongNamePop">
            <div class="close">x</div>

            <h3>Du har ikke skrevet navn rett</h3>
            
            <a href="#" class="ok">Ok</a>
        </div>

        <!-- Felmeddelande: Kan inte hämta pris på detta REGNR -->
		<div class="popup" id="noPricePop">
			<div class="close">x</div>

        	<h3>Vi kan dessverre ikke gi deg pris på denne bilen.</h3>
        	
        	<div class="description">
            	<p>Vennligst kontakt kundeservice på tif 04040 for hjelp til å forsikre den.</p>
        	</div>
        	<a href="#" class="ok">Ok</a>
    	</div>

        <!-- Popup: Skriv in regnr eller välj bilmodell -->
        <div class="popup" id="carBrandOrRegnrPop">
            <div class="close">x</div>

            <h3>Beregn pris med eller uten registreringsnummer</h3>
            
            <div class="description">
                <p>Finn bilen din ved å legge inn registreringsnummeret. Har du ikke registreringsnummeret for hånd, kan du velge «Beregn pris uten reg.nr» for å få et pristilbud. Kontakt oss hvis du ikke finner riktig modell i listen.</p>
            </div>
            <a href="#" class="ok">Ok</a>
        </div>

        <!-- Popup: Välj bilmodell -->
        <div class="popup" id="chooseCarPop">
            <div class="close">x</div>

            <h3>Vi kan inte hitta din bilmodell. Välj i listan nedan:</h3>
            
            <div class="description">    
                <select name="tilgangsorsak">
                    <option selected disabled>Velg bilmodell</option>
                    <option value="">??</option>
                    <option value="">??</option>
                    <option value="">??</option>
                    <option value="">??</option>
                </select>
                <i class="fa fa-angle-down" aria-hidden="true"></i>
            </div>
            <a href="#" class="ok">Ok</a>
        </div>

        <!-- Popup: Annan postadress -->
        <div class="popup" id="adressPop">
            <div class="close">x</div>

            <h3>Annen postadresse</h3>
            
            <div class="description">
                <p>Fyll inn adressen du ønsker å få brikken sendt til.</p>
            </div>
            <a href="#" class="ok">Ok</a>
        </div>

        <!-- Popup: Existerande bonus -->
        <div class="popup" id="bonusPop">
            <div class="close">x</div>

            <h3>Bonus</h3>
            
            <div class="description">
                <p>Bonus er en rabatt du får på ordinære bilforsikringer for å kjøre skadefritt. Bonus påvirker ikke prisen for denne bilforsikringen, men du vil likevel opparbeide en bonus i våre systemer, som du kan ta med deg videre hvis du velger det.
                Hvis du ikke har hatt bil tidligere, er startbonus 20 prosent.</p>
            </div>
            <a href="#" class="ok">Ok</a>
        </div>

        <!-- Popup: Om annan ägare -->
        <div class="popup" id="annanEierPop">
            <div class="close">x</div>

            <h3>Er bileier en annen enn forsikringstaker?</h3>
            
            <div class="description">
                <p>Hvis den som eier bilen er en annen enn den forsikringen skal stå på, må partene være samboer eller ektefelle.</p>
            </div>
            <a href="#" class="ok">Ok</a>
        </div>

        <!-- Popup: Acceptera betingelser -->
    	<div class="popup" id="bekreftBestillningPop">
			<div class="close">x</div>

        	<h3>Betingelser for kjøp på nett</h3>
        	
        	<div class="description">
            	<p>Ved kjøp av forsikring på nett mottar du alle forsikringsdokumentene elektronisk og blir «papirløs kunde». Alle dine forsikringsdokumenter er tilgjengelig på Min side. Der har du full oversikt over priser og vilkår. Varsling om endringer får du på e-post eller SMS, dette velger du selv på Min Side.<br><br>

                Ved kjøp på nett og hvis du er ny kunde hos oss, forbeholder vi oss retten til å foreta en kredittsjekk.<br><br>

                Kjøpet er inngått på grunnlag av de opplysningene du har oppgitt. Dersom disse ikke er riktige, kan det få konsekvenser for prisen eller for erstatning i en skadesak. Kontakt oss hvis du vi korrigere opplysningene som er gitt.<br><br>

                Forsikringen er løpende og fornyes automatisk hvert år. Du kan når som helst si opp forsikringen. Vi kan sende deg tips, nyheter og skadeforebyggende råd.<br><br>

                Som ny kunde betaler du månedlig via avtalegiro. Avtalegiro må opprettes ved innbetaling av første faktura i nettbanken. Om dette ikke utføres mottar du den første månedlig faktura i posten.<br><br>

                Forutsetninger for pris<br>
                - Forsikringstaker eller samboer/ektefelle er eier av bilen.<br>
                - Bilen er registrert som person/varebil.<br>
                - Bilen er i privat bruk og eie<br>
                - Bilen må være registrert i Norge.<br>
                - Bilen ikke er en russebil.<br>
                - Bilen er kompatibel med OBDII brikke</p>
        	</div>
        	<a href="#" class="ok">Ok</a>
    	</div>
		
		<div class="step1">
		  	<div class="circle">1</div> 

		  	<h2>Din informasjon</h2>

		  	<div class="group field">      
			    <input class="form-require form-ssn" type="text" name="fNumber" id="fNumber" required value="21119800363"> <!-- 21119800363 -->
				<span class="bar"></span>
				<label>Fødselsnummer</label>
				<span class="error"></span>
			</div>
		      
			<div class="group field">      
	      		<input class="form-require" type="text" name="fNamn" id="fNamn" required>
				<span class="bar"></span>
				<label>Fornavn</label>
				<span class="error"></span>
			</div>

			<div class="group field">      
				<input class="form-require" type="text" name="eNamn" id="eNamn" required>
				<span class="bar"></span>
				<label>Etternavn</label>
				<span class="error"></span>
			</div>

			<div class="group field regno">      
				<input class="form-require" type="text" name="regNr" id="regNr" required value="BS46116">
				<span class="bar"></span>
				<label>Reg.nr</label>
				<span class="error"></span>
			</div>

			<div class="bilmodell">      
				<label class="dropLabel">Velg bilmodell</label>
                <select name="bilmodell" id="bilmodell">
                    <option selected disabled>Velg bilmodell</option>
                </select>
                <i class="fa fa-angle-down" aria-hidden="true"></i>
			</div>

			<div class="group2 field">
				<input type="checkbox" class="noRegNr"> Beregn pris uten reg.nr <a href="#" class="carBrandOrRegnrQ"><i class="fa fa-question-circle-o" aria-hidden="true"></i></a>
			</div>

			<div class="group3 field">
				<input class="checkValue" type="checkbox" name="pantOrLeasing" id="pant"> Det er pant i bilen <br>
				<input class="checkValue" type="checkbox" name="pantOrLeasing" id="leasing"> Leasingbil
			</div>

		  	<div class="getPrice" id="step1Button">
		  		<span>Beregn pris!</span>
		  	</div>

		  	<p class="errorMsg">Alle felt er ikke fylt korrekt!</p>

		</div><!-- step1 -->

		<div class="step2">
			<div class="circle">2</div> 

		  	<h2>Jeg forventer å kjøre</h2>

		  	<p>Velg hvor langt du regner med å kjøre i løpet av et år. Hvis behovet endrer seg underveis, kan du justere kjørelengden på Min Side eller i Sidekick-appen. Da oppdateres også prisen på forsikringen.</p>

			<div class="slider" id="slider-2">
				<input type="text" id="example_id" name="example_name" value="" />
		  	</div>

  		</div><!-- step2 -->

  		<div class="step3">
  			<div class="circle">3</div> 

  			<h2>Velg dekningen som er passer best for deg</h2>

				<div class="kasko Delkasko">
					<h1>Delkasko</h1>

					<img src="pig.png"><br>

					<!-- <table>
						<tr>
							<th>Pris på forsikring *</th>
							<th class="right"><span id="kaskoDelYourPrice"></span>:-</th>
						</tr>
						<tr>
							<th>Trafikkforsikringsavgift</th>
							<th class="right"><span id="kaskoDelStaten"></span>:-</th>
						</tr>
						<tr>
							<th>Totalt</th>
							<th class="right"><span id="kaskoDelTotal" class="total"></span>:-</th>
						</tr>
					</table> -->

					<center><span class="prisForsikring">Pris på forsikring *</span>
					<span id="kaskoDelTotal" class="kaskoPrice">800.25:-</span><br></center>

					<div class="velgBtn" id="insuranceType3">
						<span>Velg</span>
					</div>

					<center><span class="prisUtenTFA"><i>* Dette er pris uten TFA</i></span></center>
				</div>


				<div class="kasko Normal">
					<h1>Kasko</h1>

					<img src="pig.png"><br>

					<!-- <table>
						<tr>
							<th>Pris på forsikring *</th>
							<th class="right"><span id="kaskoYourPrice"></span>:-</th>
						</tr>
						<tr>
							<th>Trafikkforsikringsavgift</th>
							<th class="right"><span id="kaskoStaten"></span>:-</th>
						</tr>
						<tr>
							<th>Totalt</th>
							<th class="right"><span id="kaskoTotal" class="total"></span>:-</th>
						</tr>
					</table> -->

					<center><span class="prisForsikring">Pris på forsikring *</span>
					<span id="kaskoTotal" class="kaskoPrice">1234.56:-</span><br></center>

					<div class="velgBtn" id="insuranceType2">
						<span>Velg</span>
					</div>

					<center><span class="prisUtenTFA"><i>* Dette er pris uten TFA</i></span></center>
				</div>		  



				<div class="kasko Extra">
					<div class="anbefaller" id="anbefallerKaskoExtra">
						<p>VÅR BESTE DEKNING</p>
					</div>

					<h1 id="ekstra">Kasko Ekstra</h1>

					<img src="pig.png"><br>

					<!-- <table>
						<tr>
							<th>Pris på forsikring *</th>
							<th class="right"><span id="kaskoExtraYourPrice"></span>:-</th>
						</tr>
						<tr>
							<th>Trafikkforsikringsavgift</th>
							<th class="right"><span id="kaskoExtraStaten"></span>:-</th>
						</tr>
						<tr>
							<th>Totalt</th>
							<th class="right"><span id="kaskoExtraTotal" class="total"></span>:-</th>
						</tr>
					</table> -->

					<center><span class="prisForsikring">Pris på forsikring *</span>
					<span id="kaskoExtraTotal" class="kaskoPrice">1599.42:-</span><br></center>

					<div class="velgBtn" id="insuranceType1">
						<span>Velg</span>
					</div>

					<center><span class="prisUtenTFA"><i>* Dette er pris uten TFA</i></span></center>
				</div>

  		</div><!-- step3 -->

  		<div class="step4">
            <div class="backStep">
                <span>Tillbake</span>
            </div>

  			<table class="tableNo1">
  				<tr>
  					<th class="tableLeft">Fødselsnummer</th>
  					<th><span id="fodelsenummer"></span></th>
  				</tr>
  				<tr>
  					<th class="tableLeft">Fornavn</th>
  					<th><span id="fornavn">William</span></th>
  				</tr>
  				<tr>
  					<th class="tableLeft">Etternavn</th>
  					<th><span id="etternavn">Magnusson</span></th>
  				</tr>
  				<tr class="adressNoEdit">
  					<th class="tableLeft">Adresse</th>
  					<th><span id="adresse"></span></th>
  				</tr>
  				<tr class="adressNoEdit">
  					<th class="tableLeft">Postnummer/Sted</th>
  					<th><span id="postnummer">5000 Bergen</span></th>
  				</tr>
  				<tr class="adressEdit">
  					<th class="tableLeft">Adresse</th>
  					<th><input id="adresseEdit" type="text"></th>
  				</tr>
  				<tr class="adressEdit">
  					<th class="tableLeft">Postnummer/Sted</th>
  					<th><input type="text" id="postnummerEdit"></th>
  				</tr>
  			</table>

  			<table class="tableNo2">
  				<tr>
  					<th class="tableLeft">Registreringsnummer</th>
  					<th><span id="carRegNr">ABC123</span></th>
  				</tr>
  				<tr>
  					<th class="tableLeft">Biltype</th>
  					<th><span id="carBrand">Toyota Yaris 1.1</span></th>
  				</tr>
  				<tr>
  					<th class="tableLeft">Årsmodell</th>
  					<th><span id="carYear">2014</span></th>
  				</tr>
  				<tr>
  					<th class="tableLeft">Jeg ønsker</th>
  					<th><span id="driveRange" value="1">18000 km pr år</span></th>
  				</tr>
  			</table>

  			<div class="editAddress">
  				<span id="annenPostadresse">Annen postadresse</span> <a href="#" class="adressQ"><i class="fa fa-question-circle-o" aria-hidden="true"></i></a>
  			</div>

        <div class="saveEditAddress" id="saveEditAddress">
          <span>Lagre endringer</span>
        </div>
  		</div>

  		<div class="step4block1">
  				
  			<h2>Din informasjon</h2>

  			<div class="group field">      
			    <input class="form-require form-phone" type="text" name="phoneNumber" id="phoneNumber" required>
				<span class="bar"></span>
			    <label>Mobilnummer</label>
				<span class="error"></span>
			</div>

			<div class="group field">      
		        <input class="form-require form-email" type="text" name="eMail" id="eMail" required>
				<span class="bar"></span>
			    <label>E-mail</label>
				<span class="error"></span>
			</div>

			<div class="group field">  
				<label class="dropLabel">Startdato for forsikring</label>    
		        <input class="form-require form-date" type="date" name="startDate" id="startDate" required>
				<span class="bar"></span>
				<span class="error"></span>
			</div>

			<div class="bonus field">
				<label class="dropLabel">Eksisterende bonus</label>
				<div class="selectWrapper">
					<select name="bonus" id="bonusDropDown" class="form-require">
						<option selected disabled>Velg</option>
						<option value="20">20% (Min første bil)</option>
						<option value="30">30%</option>
						<option value="40">40%</option>
						<option value="50">50%</option>
						<option value="60">60%</option>
						<option value="70">70% 1. år</option>
						<option value="70">70% 2. år</option>
						<option value="70">70% 3. år</option>
						<option value="70">70% 4. år</option>
						<option value="70">70% 5. år</option>
						<option value="75">75% 1. år</option>
						<option value="75">75% 2. år</option>
						<option value="75">75% 3. år</option>
						<option value="75">75% 4. år</option>
						<option value="75">75% 5. år</option>
					</select>
					<span class="error"></span>
				</div>
				<!-- <i class="fa fa-angle-down" aria-hidden="true"></i> -->
		    </div>
		    <a href="#" class="bonusQ"><i class="fa fa-question-circle-o" aria-hidden="true"></i></a>

			<div class="tillgangsorsak field">
				<label class="dropLabel">Tillgangsårsak</label>
				<div class="selectWrapper">
					<select name="tilgangsorsak" id="tilgangsorsak" class="form-require">
						<option selected disabled>Velg</option>
						<option value="firstCar">Min første bil</option>
						<option value="annetSelskap">Flyttes fra annet selskap</option>
						<option value="newCar">Bytter bil</option>
						<option value="bilPark">Utvidelse av bilpark</option>
					</select>

					<span class="error"></span>
				</div>
			</div>

            <div class="annetSelskap">
                <p>Vi ordner med oppsigelse hvis du har bilforsikringen din i et annet selskap i dag.</p>
            </div>

			<div class="overforesFra">
				<label class="dropLabel">Bonus ska overføres fra</label>
				<div class="selectWrapper">
					<select name="overforesFra" id="overforesFra">
						<option selected disabled>Velg</option>
					</select>

					<span class="error"></span>
				</div>
		    </div>

			<div class="group field leasingbolag">      
			    <input id="leasingbolag" type="text" required>
				<span class="bar"></span>
			    <label>Panthaver/Leasingfirma</label>
			</div>

			<div class="annanEier">
			    <input type="checkbox" name="otherEier" class="otherEier" /> Har bilen en annen eier? <a href="#" class="annanEierQ"><i class="fa fa-question-circle-o" aria-hidden="true"></i></a>
			</div>

  		</div><!-- block1 -->

  		<div class="step4block2">
  					
			<h2>Du har valgt</h2>

  			<div class="choosenKasko">	
  					
  				<h1><span id="choosenKaskoh1"></span></h1>

                <img src="pig.png">

	  			<!-- <table>
	  				<tr>
	  					<th>Pris på forsikring</th>
	  					<th class="right"><span id="choosenYourPrice"></span>:-</th>
	  				</tr>
	  				<tr>
	  					<th>Trafikkforsikringsavgift</th>
	  					<th class="right"><span id="choosenStatenPrice"></span>:-</th>
	  				</tr>
	  				<tr>
	  					<th>Totalt</th>
	  					<th class="right"><span id="choosenTotal" class="total"></span>:-</th>
	  				</tr>
	  			</table> -->

	  			<center><span class="prisForsikring">Pris på forsikring *</span>
				<span id="kaskoExtraTotal" class="kaskoPrice">1599.42:-	</span><br></center>

				<center><span class="prisUtenTFA"><i>* Dette er pris uten TFA</i></span></center>
  			</div>
  		</div><!-- block2 -->

  		<div class="omAnnanEier">
  			<div class="group field">      
				  <input type="text" name="annanEierFname" id="annanEierFname" required>
			    <span class="bar"></span>
			    <label>Fornavn</label>
			  </div>

  			<div class="group field">      
  				<input type="text" name="annanEierLname" id="annanEierLname" required>
  			    <span class="bar"></span>
  				<label>Etternavn</label>
  			</div>

  			<div class="group field">      
  				<input type="text" name="annanEierAddress" id="annanEierPn" required>
  			    <span class="bar"></span>
  			    <label>Fødselsnummer</label>
  			</div>
  		</div>

  		<div class="order">

  			<input type="checkbox"> <i>Jeg godtar betingelsene for kjøp på nett og samtykker til å bli elektronisk kunde</i> <a href="#" class="bekreftBestillningQ"><i class="fa fa-question-circle-o" aria-hidden="true"></i></a>

			<div class="orderIns" id="acceptOrder">
		  		<span>Bekreft bestilling</span>
			</div>  					

  		</div>
	</div><!-- container -->

	<script src="dist/js/script.js"></script>	
</body>
</html>