const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');

var bourbon = require('node-bourbon').includePaths[0];
var neat = require('node-neat').includePaths[0];

console.log(neat);

require('file-loader');
require('image-webpack-loader');

const ExtractTextPlugin = require("extract-text-webpack-plugin");

const extractSass = new ExtractTextPlugin({
    filename: "[name].[contenthash].css",
    disable: process.env.NODE_ENV === "development"
});

module.exports = {
    context: path.resolve(__dirname, './src'),
    entry: {
        app: ['./app.js']
    },
    output: {
        path: path.resolve(__dirname, './build/assets'),
        filename: '[name].bundle.js',
        publicPath: '/'
    },
    devServer: {
        contentBase: path.resolve(__dirname, './src')
    },

    module: {
        rules: [{
            test: /\.(gif|png|jpe?g|svg)$/i,
            use: [{
                loaders: [
                    'file-loader?hash=sha512&digest=hex&name=[hash].[ext]',
                    'image-webpack-loader'
                ]
            }]
        } ]
    },

    module: {
        rules: [{
                test: /\.js$/,
                exclude: [/node_modules/],
                use: [{
                    loader: 'babel-loader',
                    options: {presets: ['es2015']}
                }]
            }]
    },

    module: {
        rules: [{
            test: /\.scss$/,
            exclude: [/node_modules/],
            use: [{
                loader: "style-loader" // creates style nodes from JS strings
            }, {
                loader: "css-loader" // translates CSS into CommonJS
            }, {
                loader: "sass-loader", // compiles Sass to CSS,
                options: {
                    includePaths: [bourbon, neat]
                }
            }]
        }]
    },

    plugins: [
        extractSass

    ]
}