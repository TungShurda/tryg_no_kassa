'use strict';
 
var gulp        = require('gulp'),
    sass        = require('gulp-sass'),
    source      = require('vinyl-source-stream'),
    browserify  = require('browserify'),
    watchify    = require('watchify'),
    neat        = require('node-neat').includePaths,
    nouislider  = require('nouislider').includePaths,
    component   = require('gulp-component'),
    stylus      = require('component-stylus-plugin'),
    livereload  = require('gulp-livereload'),
    rubySass    = require('gulp-ruby-sass'),
    notify      = require("gulp-notify"),
    bower       = require('gulp-bower'),
    minify      = require('gulp-minify'),
    replace     = require('gulp-string-replace'),
    babelify    = require('babelify'),
    babel       = require('gulp-babel'),
    rename      = require("gulp-rename"),
    prompt      = require("gulp-prompt"),
    flatpickr   = require("flatpickr"),
    argv        = require('yargs').argv;

var config = {
    bowerDir: './bower_components',
    appAssetsDir: './app/enerfy-mainShop/assets',
    src: 'src'
};

gulp.task('sass', function () {
  return gulp.src('./src/style/**/*.scss')
    .pipe(sass({
        includePaths: require('node-neat').includePaths
    }).on('error', sass.logError))
    .pipe(gulp.dest('./dist/style'));
});
 
gulp.task('build-js', function() {
  
    browserify({
    extensions: ['.jsx', '.js'],
    debug: true,
    entries: [
        "src/js/script.js"
    ]
    })
    .transform(babelify.configure({ 
        presets: ['es2015'],
        ignore: /(bower_components)|(node_modules)/
    }))
    .bundle()
    .on("error", function (err) { console.log("Error : " + err.message); })
    .pipe(source( "script.js" ))
    .pipe(gulp.dest('./dist/js/'));
      
  
});

gulp.task('bower', function() {
    return bower().pipe(gulp.dest(config.bowerDir))
});

gulp.task('icons', function() {
    return gulp.src(config.bowerDir + '/fontawesome/fonts/**.*')
        .pipe(gulp.dest(config.appAssetsDir + '/font-awesome/fonts'));
});

gulp.task('sass:watch', function () {
  gulp.watch('./src/style/**/*.scss', ['sass']);
});


gulp.task('watch', function () {
  gulp.watch('./src/style/**/*.scss', ['sass']);
  gulp.watch("src/js/*.js", ["build-js"]);
});

var srcPath = "node_modules/flatpickr/dist/flatpickr.min.css";
var buildPath = "dist/style/";
gulp.task("build-css", function () {
    return gulp.src(srcPath).pipe(gulp.dest(buildPath));
});

gulp.task('build', ['sass', 'build-js', 'build-css']);

